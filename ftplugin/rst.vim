""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Preview commands
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Preview with pando
if executable('pando')
    nnoremap  <buffer> ~~~           <Esc>:w <Bar> ! pando -f rst %<CR>
endif

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Insertions
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Internal anchor
nnoremap <buffer> <leader>t {O<Enter>.. _:<Left>

" Footnote adder
nnoremap <buffer> <leader>f :call <SID>FootnoteAdder()<CR>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"" Environment modes
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"inoremap <buffer> .i        .. image:: /img/
"inoremap <buffer> .w        .. warning::<CR><CR>
"inoremap <buffer> .n        .. note::<CR><CR>

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" In text formatting
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Bolder
nnoremap <buffer> <leader>b :set operatorfunc=<SID>Bolder<CR>g@
vnoremap <buffer> <leader>b :<c-u>call <SID>Bolder(visualmode())<CR>

" Italicizer
nnoremap <buffer> <leader>i :set operatorfunc=<SID>Italicizer<CR>g@
vnoremap <buffer> <leader>i :<c-u>call <SID>Italicizer(visualmode())<CR>

" Linker
nnoremap <buffer> <leader>l :set operatorfunc=<SID>Linker<CR>g@
vnoremap <buffer> <leader>l :<c-u>call <SID>Linker(visualmode())<CR>

" List formatter
vnoremap <buffer> <leader>u :g/[a-z]/normal! ^i* <CR>:nohlsearch<CR>

" Table aligner
if exists(':EasyAlign')
    vnoremap <buffer> <leader>, :EasyAlign *,
endif

" Headings 
nnoremap <buffer> <leader>h1 VypVr=k
nnoremap <buffer> <leader>h2 VypVr-k
nnoremap <buffer> <leader>h3 VypVr~k
nnoremap <buffer> <leader>h4 VypVr^k
nnoremap <buffer> <leader>h5 VypVr.k


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Functions
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Function to bold things
function! s:Bolder(type)
    " Adding user register contents to variable
    let l:saved_unnamed_register = @@
    if a:type ==# 'v'
        execute "normal! `>a**\<Esc>`<i**"
    elseif a:type ==# 'V'
        execute "normal! `>a**\<Esc>`<i**"
    elseif a:type ==# 'char'
        execute "normal! `[v`]\<Esc>`>a**\<Esc>`<i**"
    elseif a:type ==# 'line'
        execute "normal! `[v`]\<Esc>`>A**\<Esc>`<i**"
    else
        return
    endif     
    " Restoring user register contents
    let @@ = l:saved_unnamed_register
endfunction

" Function to italicize things
function! s:Italicizer(type)
    " Adding user register contents to variable
    let l:saved_unnamed_register = @@
    if a:type ==# 'v'
        execute "normal! `>a*\<Esc>`<i*"
    elseif a:type ==# 'V'
        execute "normal! `>a*\<Esc>`<i*"
    elseif a:type ==# 'char'
        execute "normal! `[v`]\<Esc>`>a*\<Esc>`<i*"
    elseif a:type ==# 'line'
        execute "normal! `[v`]\<Esc>`>A*\<Esc>`<i*"
    else
        return
    endif     
    " Restoring user register contents
    let @@ = l:saved_unnamed_register
endfunction

" Function to link things
function! s:Linker(type)
    " Adding user register contents to variable
    let l:saved_unnamed_register = @@
    if a:type ==# 'v'
        let l:command = "normal! "
        let l:command = l:command . "gvy"
        let l:command = l:command . "`>a`_"
        let l:command = l:command . "\<Esc>`<i`"
        let l:command = l:command . "\<Esc>}o\<CR>"
        let l:command = l:command . "\<Up>.. _\<Esc>pA: "
        execute l:command
        startinsert!
    elseif a:type ==# 'char'
        let l:command = "normal! "
        let l:command = l:command . "`[v`]y"
        let l:command = l:command . "\<Esc>`>a`"
        let l:command = l:command . "\<Esc>`<i`"
        let l:command = l:command . "\<Esc>}o\<CR>"
        let l:command = l:command . "\<Up>.. _\<Esc>pA: "
        execute l:command
        startinsert!
    else
        return
    endif     
    " Restoring user register contents
    let @@ = l:saved_unnamed_register
endfunction

" Function to insert a footnote
function! s:FootnoteAdder()
    let footnote_name=input('Type the name of the footnote: ')
    execute "normal! i[#\<c-r>=footnote_name\<cr>]_\<esc>}o\<cr>\<Up>.. [#\<c-r>=footnote_name\<cr>] \<esc>l"
    startinsert!
endfunction 
