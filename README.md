# vim-restructured-text

## Optional dependencies

* pando

    To preview rst files.

* [junegunn/vim-easy-align]

    For `<leader>|` to align tables.

## Setup

Set `<leader>` to be a key of your choice. It is `\` by default.

## Usage

| Key             | Action                                                                                                                                      | Modes          |
| ---             | ---                                                                                                                                         | ---            |
| `~~~`           | Previews the file. The file is written before being passed to pando. *Optional*                                                             | Normal         |
| `<leader>t`     | Inserts internal anchor. Use this on a section you want to link to and give it a name. Then link to it using `#anchor-name` as the address. | Normal         |
| `<leader>f`     | Inserts a footnote at the current location.                                                                                                 | Normal         |
| `<leader>b`     | Make text bold. Takes an operator in normal mode.                                                                                           | Normal, Visual |
| `<leader>i`     | Make text italic. Takes an operator in normal mode.                                                                                         | Normal, Visual |
| `<leader>l`     | Make selected text into a hyperlink. Takes an operator in normal mode.                                                                      | Normal, Visual |
| `<leader>u`     | Formats the visual selection from plain text to a list. Preserves indentation                                                               | Visual         |
| `<leader>,` | Aligns `,` delimited csv tables. | Visual |
| `<leader>h1`    | Makes current line into a header with level 1                                                                                               | Normal         |
| `<leader>h2`    | Makes current line into a header with level 2                                                                                               | Normal         |
| `<leader>h3`    | Makes current line into a header with level 3                                                                                               | Normal         |
| `<leader>h4`    | Makes current line into a header with level 4                                                                                               | Normal         |
| `<leader>h5`    | Makes current line into a header with level 5                                                                                               | Normal         |

[junegunn/vim-easy-align]: https://github.com/junegunn/vim-easy-align

